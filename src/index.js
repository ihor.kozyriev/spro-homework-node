const fs = require('fs');
const path = require('path');

function writeToFile() {
    const content = process.argv.slice(3).join(' ');
    const fileName = process.argv[2];
    const filePath = path.join(process.cwd(), fileName);

    fs.appendFile(filePath, content, error => {
        if (error) throw error;
    });
}

writeToFile();
